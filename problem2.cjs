const fs = require("fs");

function multipleTask() {
    let filePath = "lipsum_1.txt";
    fs.readFile(filePath, 'utf-8', (err, file) => {
        if (err) {
            console.log(err);
        }
        else {
            let convertToUpperCase = file.toUpperCase();
            // convertToUpperCase = JSON.stringify(convertToUpperCase);
            let uppercaseTextFilename = 'convertedToUppercase.txt';
            let allFileNameContainer = "nameOfAllFiles.txt";
            fs.writeFile(uppercaseTextFilename, convertToUpperCase, (err) => {
                if (err) {
                    console.log(err);

                } else {
                    fs.writeFile(allFileNameContainer, 'convertedToUppercase.txt ', (err) => {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            fs.readFile(uppercaseTextFilename, 'utf-8', (err, file2) => {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    console.log("Uppercase file created successfully");
                                    let convertToLowercase = file2.toLowerCase();
                                    let sentences = convertToLowercase.split('.');
                                    let lowercaseTextFilename = 'convertedToLowercase.txt';



                                    let trimmedSentences = sentences.map((sentence) => sentence.trim()).join('\n');

                                    fs.writeFile(lowercaseTextFilename, trimmedSentences, (err) => {
                                        if (err) {
                                            console.log(err);
                                        }
                                        else {

                                            console.log("lowerCase file created successfully");

                                            fs.writeFile(allFileNameContainer, 'convertedToLowercase.txt ', { flag: "a" }, (err) => {
                                                if (err) {
                                                    console.log(err);
                                                }
                                                else {
                                                    fs.readFile(lowercaseTextFilename, "utf-8", (err, file3) => {
                                                        if (err) {
                                                            console.log(err);
                                                        }
                                                        else {
                                                            let contentOfLowerCase = file3
                                                            let splittedSentences2 = contentOfLowerCase.split('\n');

                                                            splittedSentences2.sort();



                                                            let sortedContent = splittedSentences2.map((sentence2) => { if (sentence2 != '') { return sentence2 } }).join("\n");

                                                            let sortedContentFile = 'sortedContentFile.txt';
                                                            fs.writeFile(sortedContentFile, sortedContent, (err) => {
                                                                if (err) {
                                                                    console.log(err);

                                                                } else {
                                                                    console.log("File of sorted content created successfully");
                                                                    fs.writeFile(allFileNameContainer, 'sortedContentFile.txt', { flag: "a" }, (err) => {
                                                                        if (err) {
                                                                            console.log(err);
                                                                        }
                                                                        else {
                                                                            fs.readFile(allFileNameContainer, 'utf-8', (err, file4) => {
                                                                                if (err) {
                                                                                    console.log(err);
                                                                                }
                                                                                else {
                                                                                    let fileNames = file4.split(' ');
                                                                                    fileNames.forEach((file) => {
                                                                                        fs.unlink(file, (err) => {
                                                                                            if (err) {
                                                                                                console.log(err);
                                                                                            }
                                                                                            else {
                                                                                                console.log(`${file} deleted successfully`);
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })

                                                                }
                                                            })

                                                        }
                                                    })
                                                }
                                            })


                                        }
                                    })

                                }



                            })

                        }
                    })


                }
            })
        }
    })

}

module.exports = multipleTask;